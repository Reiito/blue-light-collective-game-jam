﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yarn.Unity;

public enum GameState { MONOLOGUE, COMING_IN, GAME_CONTROL, PLAYER_CONTROL, };

public class GameController : MonoBehaviour
{
  public GameState gameState { get; set; }
  //public DialogueRunner dialogueRunner;

  private void Awake()
  {
    gameState = GameState.MONOLOGUE;
  }

  private void Update()
  {
    //if (dialogueRunner.isDialogueRunning)
    //{
    //  gameState = GameState.TALKING;
    //}
    //else
    //{
    //    gameState = GameState.PLAYING;
    //}
    switch (gameState)
    {
      default:
        break;
    }
  }
}
