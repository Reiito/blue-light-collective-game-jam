﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Yarn.Unity;

public class InventoryManager : MonoBehaviour
{
  public Image[] itemSlots = new Image[3];
  public Sprite[] items = new Sprite[8];

  public bool kanyeFound = false;

  VariableStorage variableStorage;

  private void Awake()
  {
    variableStorage = GetComponent<VariableStorage>();
  }

  private void Update()
  {
    List<string> pickedUp = CheckIfThere();
    if (pickedUp.Count != 0)
    {
      for (int i = 0; i < pickedUp.Count; i++)
      {
        switch (pickedUp[i])
        {
          case "$has_ring":
            itemSlots[i].sprite = items[0];
            break;
          case "$has_note":
            itemSlots[i].sprite = items[1];
            break;
          case "$has_cart":
            itemSlots[i].sprite = items[2];
            break;
          case "$has_coin":
            itemSlots[i].sprite = items[3];
            break;
          case "$has_coffee":
            itemSlots[i].sprite = items[4];
            break;
          case "$has_key":
            itemSlots[i].sprite = items[5];
            break;
          case "$has_kanye":
            kanyeFound = true;
            itemSlots[i].sprite = items[6];
            break;
          default:
            itemSlots[i].sprite = null;
            break;
        }
      }
    }
  }

  List<string> CheckIfThere()
  {
    List<string> pickedUp = new List<string>();

    foreach (KeyValuePair<string,Yarn.Value> n in variableStorage.variables)
    {
      if (n.Value.AsBool)
      {
        Debug.Log(n.Key);
        pickedUp.Add(n.Key);
      }
    }

    return pickedUp;
  }
}
