﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainAnimation : MonoBehaviour
{
  public GameController gameController;
  public GameObject wall;
  public GameObject lDoor;
  public GameObject rDoor;

  // opening sequence variables
  Vector3 startPos = new Vector3(-50.0f, 2.5f, 2.75f);
  float inSpeed = 1.0f;
  float outSpeed = 0.1f;
  float stoppingDistance = 0.5f;
  float outRate = 0.002f;
  bool startEnter = false;
  bool delayFinished = false;
  bool startLeave = false;

  bool closeDoors = false;
  bool finished = false;
  float lDoorEndPos = -0.8f;
  float rDoorEndPos = 0.4f;
  float doorSpeed = 2f;

  WallColor wallColor;

  private void Awake()
  {
    transform.position = startPos;
    wallColor = GetComponentInChildren<WallColor>();
  }

  private void Update()
  {
    switch (gameController.gameState)
    {
      case GameState.MONOLOGUE:
        StartCoroutine(StartDelay());
        break;
      case GameState.COMING_IN:
        MoveTrain(false, inSpeed);
        break;
      case GameState.GAME_CONTROL:
        startLeave = true;
        break;
      default:
        break;
    }

    if (startLeave)
    {
      StartCoroutine(TrainDelay());
    }

    if (closeDoors && !finished)
    {
      lDoor.transform.position = new Vector3(Mathf.Lerp
      (
        lDoor.transform.position.x, lDoorEndPos, Time.deltaTime * doorSpeed),
        lDoor.transform.position.y,
        lDoor.transform.position.z
      );
      rDoor.transform.position = new Vector3(Mathf.Lerp
      (
        rDoor.transform.position.x, rDoorEndPos, Time.deltaTime * doorSpeed),
        rDoor.transform.position.y,
        rDoor.transform.position.z
      );
    }
  }

  void MoveTrain(bool leaving, float speed)
  {
    // coming in
    float endPos = 0f;

    if (leaving)
    {
      endPos = 100.0f;
    }

    transform.position = new Vector3
    (
      Mathf.Lerp(transform.position.x, endPos, Time.deltaTime * speed),
      transform.position.y,
      transform.position.z
    );

    if (Mathf.Abs(transform.position.x - endPos) <= stoppingDistance && !leaving)
    {
      gameController.gameState = GameState.GAME_CONTROL;
    }
  }

  IEnumerator StartDelay()
  {
    yield return new WaitForSeconds(5.0f);
    gameController.gameState = GameState.COMING_IN;
  }

  IEnumerator TrainDelay()
  {
    StopCoroutine(StartDelay());
    // wall apear
    yield return new WaitForSeconds(0.3f);
    wallColor.start = true;

    // train doors
    yield return new WaitForSeconds(0.5f);
    closeDoors = true;

    // leave
    yield return new WaitForSeconds(2.0f);
    closeDoors = false;
    MoveTrain(true, outSpeed);
    outSpeed += outRate;

    finished = true;
  }
}
