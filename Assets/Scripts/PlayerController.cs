﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Yarn.Unity;

public class PlayerController : MonoBehaviour
{
  public GameController gameController;
  public GameObject bin;
  public GameObject cameraObj;
  public DialogueRunner dialogueRunner;

  Camera camera;
  private NavMeshAgent navMeshAgent;
  private float triggerDistance = 4.0f;
  float cameraSize;

  private void Awake()
  {
    camera = cameraObj.GetComponent<Camera>();
    cameraSize = camera.orthographicSize;
    navMeshAgent = GetComponent<NavMeshAgent>();
    navMeshAgent.enabled = false;
  }

  private void Update()
  {
    if (gameController.gameState == GameState.PLAYER_CONTROL)
    {
      navMeshAgent.enabled = true;
    }

    if (Input.GetMouseButtonDown(0) && gameController.gameState == GameState.PLAYER_CONTROL)
    {
      RaycastHit hit;

      if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100))
      {
        if (hit.collider.CompareTag("NPC") && !dialogueRunner.isDialogueRunning)
        {
          hit.collider.SendMessageUpwards("Talk", SendMessageOptions.RequireReceiver);
          dialogueRunner.StartDialogue();
        }
        else if (!dialogueRunner.isDialogueRunning)
        {
          navMeshAgent.destination = hit.point;
        }
      }
    }

    if (bin != null)
    {
      if (Vector3.Distance(transform.position, bin.transform.position) < triggerDistance)
      {
        float scl = Vector3.Distance(transform.position, bin.transform.position) / triggerDistance;
        camera.orthographicSize = Vector3.Distance(transform.position, bin.transform.position) / (triggerDistance / cameraSize);
        transform.localScale = new Vector3(scl, scl, scl);
      }
    }
  }
}
